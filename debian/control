Source: avogadrolibs
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), cmake,
               debhelper-compat (= 13),
               dh-python,
               libarchive-dev,
               libeigen3-dev (>> 3.3),
               libgl2ps-dev,
               libglew-dev,
               libglvnd-dev,
               libhdf5-dev,
               libjs-mathjax,
               libsymspg-dev,
               molequeue, libmolequeue-dev,
               openbabel,
               pkgconf,
               python3-dev,
               python3-pybind11,
               python3-skbuild,
               python3-wheel,
               qt6-base-dev,
               libqt6opengl6-dev,
               libqt6svg6-dev,
               qt6-tools-dev,
               zlib1g-dev
Build-Depends-Indep: doxygen,
 graphviz
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debichem-team/avogadrolibs
Vcs-Git: https://salsa.debian.org/debichem-team/avogadrolibs.git
Homepage: https://avogadro.cc/

Package: avogadro-utils
Architecture: any
Depends: libavogadro2-1t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: avogadro (>= 1.90)
Replaces: avogadro (<< 1.90)
Breaks: avogadro (<< 1.90)
Description: Molecular Graphics and Modelling System (library)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides avogadro utility programs:
   avobabel
   avocjsontocml
   bodrparse
   encodefile
   qube
   resdataparse

Package: libavogadro2-1t64
Provides: ${t64:Provides}
Replaces: libavogadro2-1
Breaks: libavogadro2-1 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: libavogadro-data, python3:any, python3-cclib
Description: Molecular Graphics and Modelling System (library)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides the shared libraries, plugins and functionality scripts.

Package: libavogadro-dev
Architecture: any
Section: libs
Multi-Arch: same
Depends: libavogadro2-1t64 (= ${binary:Version}),
         libeigen3-dev,
         libglew-dev,
         libglvnd-dev,
         openbabel,
         ${misc:Depends}
Recommends: molequeue
Replaces: libavogadro2-1t64 (<< 1.97.0-3)
Breaks: libavogadro2-1t64 (<< 1.97.0-3)
Description: Molecular Graphics and Modelling System (development files)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides the development and header files.

Package: libavogadro-data
Architecture: all
Section: libdevel
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Molecular Graphics and Modelling System (data files)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides molecule and crystal data for the
 Insert Fragment plugin.

Package: libavogadro-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-mathjax, ${misc:Depends}
Suggests: libavogadro-dev, python3-avogadro
Description: Molecular Graphics and Modelling System (lib documentation)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides the documentation for libavogadro.

Package: python3-avogadro
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Description: Molecular Graphics and Modelling System (Python 3 module)
 Avogadro is a molecular graphics and modelling system targeted at small
 to medium molecules.  It can visualize properties like molecular orbitals or
 electrostatic potentials and features an intuitive molecular builder.
 .
 This package provides the Python 3 module.
